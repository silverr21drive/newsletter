'use strict';

var handlebars = require('handlebars'),
  fs = require('fs'),
  path = require('path');

var newsletters = [];
fs.readdir('./public/newsletters', (err, files) => {
  files.forEach(file => {
    var date = path.basename(file, path.extname(file));
    newsletters.push("<a href=\"newsletters/" + file + "\">" + date + "</a>");
  });
})

var pdfs = [];
fs.readdir('./public/pdfs', (err, files) => {
  files.forEach(file => {
    var date = path.basename(file, path.extname(file));
    pdfs.push("<a href=\"pdfs/" + file + "\">" + date + "</a>");
  });
})

var data = {
  title: 'Non-profit Newsletters',
  newsletters: newsletters,
  pdfs: pdfs
};
data.body = process.argv[2];

fs.readFile('index/index.html', 'utf-8', function(error, source){
  handlebars.registerHelper('newsletter', function() {        
    return new handlebars.SafeString(
      this
    );
  });

  handlebars.registerHelper('pdf', function() {        
    return new handlebars.SafeString(
      this
    );
  });

  var template = handlebars.compile(source);
  var html = template(data);
  fs.writeFile("./public/index.html", html, function(err) {
    if(err) {
        return console.log(err);
    }
  }); 
});
